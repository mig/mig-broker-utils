#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: tcptop                                                                 #
#                                                                              #
# Description: show the top usage of TCP connections                           #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use JSON qw(to_json);
use Getopt::Long qw(GetOptions);
use No::Worries::Die qw(handler dief);
use No::Worries::File qw(file_read);
use No::Worries::Log qw(log_debug log_filter);
use No::Worries::String qw(string_table);
use Pod::Usage qw(pod2usage);
use Socket qw(:addrinfo inet_ntoa);

#
# variables
#

our(%Option, @StateName, %Resolved, %Seen);

#
# initialize
#

sub init () {
    $| = 1;
    $Option{count} = 2;
    $Option{debug} = 0;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option,
        "count|c=i",
        "debug|d+",
        "help|h|?",
        "ipv4|4",
        "ipv6|6",
        "json|j",
        "manual|m",
        "numeric|n",
        "state|s=s",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) if @ARGV;
    dief("options --ipv4 and --ipv6 are mutually exclusive!")
        if $Option{ipv4} and $Option{ipv6};
    log_filter("debug") if $Option{debug};
    # TCP state names from /usr/include/linux/tcp.h
    $StateName[ 1] = "ESTABLISHED";
    $StateName[ 2] = "SYN_SENT";
    $StateName[ 3] = "SYN_RECV";
    $StateName[ 4] = "FIN_WAIT1";
    $StateName[ 5] = "FIN_WAIT2";
    $StateName[ 6] = "TIME_WAIT";
    $StateName[ 7] = "CLOSE";
    $StateName[ 8] = "CLOSE_WAIT";
    $StateName[ 9] = "LAST_ACK";
    $StateName[10] = "LISTEN";
    $StateName[11] = "CLOSING";
    $StateName[12] = "MAX_STATES";
    # special <any> addresses
    $Resolved{"0.0.0.0"} = "*";
    $Resolved{"0:0:0:0:0:0:0:0"} = "*";
}

#
# (maybe) resolve an IP address (with caching)
#

sub resolve ($) {
    my($ip) = @_;
    my($error, @result, $name);

    unless ($Resolved{$ip}) {
        $error = $Option{numeric};
        unless ($error) {
            ($error, @result) = getaddrinfo($ip, 0, {
                protocol => Socket::IPPROTO_TCP,
            });
        }
        unless ($error or @result != 1) {
            ($error, $name) = getnameinfo($result[0]{addr}, NI_NAMEREQD);
        }
        $Resolved{$ip} = $error ? $ip : $name;
    }
    return($Resolved{$ip});
}

#
# digest TCP information from /proc
#

sub digest ($) {
    my($path) = @_;

    return unless -e $path;
    log_debug("reading %s...", $path);
    foreach my $line (split(/\n/, file_read($path))) {
        if ($line =~ /^\s*\d+:\s+(\S+)\s+(\S+):\S+\s+(\w{2})\s/) {
            $Seen{$1}{$2}{$3}++;
        }
    }
}

#
# format an hexadecimal IP address
#

sub ip4 ($) {
    my($hex) = @_;

    return(inet_ntoa(pack("L", hex($hex))));
}

sub ip6 ($) {
    my($hex) = @_;
    my($bin, @list);

    if ($hex =~ /^0000000000000000ffff0000([0-9a-f]{8})$/i) {
        return(ip4($1));
    }
    $bin = pack("C*", map(hex($_), $hex =~ /../g));
    @list = unpack("L*", $bin);
    dief("unexpected hex address: %s", $hex) unless @list == 4;
    return(join(":", map(sprintf("%x:%x", $_ >> 16, $_ & 0xffff), @list)));
}

sub ip ($) {
    my($hex) = @_;

    return(ip4($hex)) if $hex =~ /^([0-9a-f]{8})$/i;
    return(ip6($hex)) if $hex =~ /^([0-9a-f]{32})$/i;
    dief("unexpected hex address: %s", $hex);
}

#
# format an hexadecimal port
#

sub port ($) {
    my($hex) = @_;

    return(hex($hex)) if $hex =~ /^[0-9a-f]{4}$/i;
    dief("unexpected hex port: %s", $hex);
}

#
# format an endpoint (either ip or ip:port)
#

sub ep ($) {
    my($string) = @_;

    # ip
    if ($string =~ /^([0-9a-f]+)$/i) {
        return(ip($1));
    }
    # ip:port
    if ($string =~ /^([0-9a-f]+):([0-9a-f]+)$/i) {
        return(ip($1), hex($2));
    }
    dief("unexpected endpoint: %s", $string);
}

#
# report what has been seen
#

sub report () {
    my(@lines, $count, $state, @header, @align, @data);

    foreach my $loc (keys(%Seen)) {
        foreach my $rem (keys(%{ $Seen{$loc} })) {
            foreach my $st (keys(%{ $Seen{$loc}{$rem} })) {
                $count = $Seen{$loc}{$rem}{$st};
                next unless $count >= $Option{count};
                $state = $StateName[hex($st)] || "???";
                next if $Option{state} and $state ne $Option{state};
                push(@lines, [ ep($loc), ep($rem), $state, $count ]);
            }
        }
    }
    unless ($Option{numeric}) {
        foreach my $line (@lines) {
            $line->[0] = resolve($line->[0]);
            $line->[2] = resolve($line->[2]);
        }
    }
    @lines = sort({
        $a->[4] <=> $b->[4]
                ||
        $a->[0] cmp $b->[0]
                ||
        $a->[1] <=> $b->[1]
    } @lines);
    if ($Option{json}) {
        foreach my $line (@lines) {
            push(@data, {
                "laddr" => $line->[0],
                "lport" => $line->[1],
                "raddr" => $line->[2],
                "state" => $line->[3],
                "count" => $line->[4],
            });
        }
        print(to_json(\@data), "\n");
    } else {
        @header = ("Loc Address", "Loc Port", "Rem Address", "State", "Count");
        @align = qw(right right right left right);
        print(string_table(\@lines, header => \@header, align => \@align));
    }
}

#
# just do it
#

init();
digest("/proc/net/tcp") unless $Option{ipv6};
digest("/proc/net/tcp6") unless $Option{ipv4};
report();

__END__

=head1 NAME

tcptop - show the top usage of TCP connections

=head1 SYNOPSIS

B<tcptop> [I<OPTIONS>]

=head1 DESCRIPTION

This program looks at all the TCP connections, group them by local endpoint,
remote address and state and sort the results by count. This allows to quickly
find out which remote address is responsible for a high number of connections
of a given state (ex: too many established connections).

=head1 OPTIONS

=over

=item B<--count>, B<-c> I<INTEGER>

only consider the entries with at least this number of connections
(default: 2)

=item B<--debug>, B<-d>

show debugging information

=item B<--help>, B<-h>

show some help

=item B<--ipv4>, B<-4>

only consider IPv4 addresses

=item B<--ipv6>, B<-6>

only consider IPv6 addresses

=item B<--json>, B<-j>

print JSON instead of tabular output

=item B<--manual>, B<-m>

show this manual

=item B<--numeric>, B<-n>

show numerical addresses instead of trying to resolve them to host names

=item B<--state>, B<-s> I<NAME>

only consider connections in the given state (e.g. C<ESTABLISHED>)

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2011-2023
