PKGNAME=mig-broker-utils
PKGVERSION=$(shell cat VERSION)
PKGTAG=v${PKGVERSION}
PKGFULLNAME=${PKGNAME}-${PKGVERSION}
DISTDIR=.tempdir
DISTPKGDIR=${DISTDIR}/${PKGFULLNAME}
TARBALL=${PKGFULLNAME}.tgz
NAMES=amqls brkpurge cern2dns jolokia jsonpp logpurge pem2jks procstat tcptop ugm

INSTROOT=
BINDIR=/usr/bin
MANDIR=/usr/share/man

.PHONY: sources build install tag clean

${TARBALL}: clean
	mkdir -p ${DISTPKGDIR}
	cp -a * ${DISTPKGDIR}
	cd ${DISTDIR}; tar cfz ../${TARBALL} ${PKGFULLNAME}
	rm -fr ${DISTDIR}

sources: ${TARBALL}

build:
	@for name in ${NAMES}; do \
	  pod2man --section=1 $$name > $$name.1; \
	done

install:
	@for name in ${NAMES}; do \
	  install -D -m 755 $$name ${INSTROOT}${BINDIR}/$$name; \
	  install -D -m 644 $$name.1 ${INSTROOT}${MANDIR}/man1/$$name.1; \
	done

tag:
	@seen=`git tag -l | grep -Fx ${PKGTAG}`; \
	if [ "x$$seen" = "x" ]; then \
	  set -x; \
	  git tag ${PKGTAG}; \
	  git push --tags; \
	else \
	  echo "already tagged with ${PKGTAG}"; \
	fi

clean:
	rm -fr *~ *.1 ${DISTDIR} ${TARBALL}
