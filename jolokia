#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: jolokia                                                                #
#                                                                              #
# Description: thin client to access a Jolokia agent                           #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use sigtrap qw(die normal-signals);
use Getopt::Long qw(GetOptions);
use JSON qw();
use LWP::UserAgent qw();
use No::Worries::Die qw(dief handler);
use No::Worries::File qw(file_update);
use No::Worries::Log qw(log_debug log_filter);
use Pod::Usage qw(pod2usage);

#
# variables
#

our($JSON, %Option, $UserAgent);

#
# initialize
#

sub init () {
    $| = 1;
    $Option{debug} = 0;
    $Option{pretty} = 0;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option,
        "debug|d+",
        "help|h|?",
        "manual|m",
        "output|o=s",
        "pretty|p+",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) unless @ARGV > 1;
    log_filter("debug") if $Option{debug};
    $Option{pretty} = 1 if $Option{debug};
    $JSON = JSON->new()->canonical(1)->pretty($Option{pretty})->utf8(1);
    $UserAgent = LWP::UserAgent->new();
}

#
# execute a Jolokia request (low-level)
#

sub execute ($$) {
    my($url, $data) = @_;
    my($req, $res, $output);

    $req = HTTP::Request->new(POST => $url);
    $req->content($JSON->encode($data));
    log_debug("Jolokia request: %s", $JSON->encode($data))
        if $Option{debug} > 1;
    $res = $UserAgent->request($req);
    dief("POST %s failed: %s", $url, $res->status_line())
        unless $res->is_success();
    $data = $JSON->decode($res->content());
    log_debug("Jolokia response: %s", $JSON->encode($data))
        if $Option{debug} > 1;
    dief("invalid Jolokia response: %s", $res->content())
        unless $data->{status};
    unless ($data->{status} == 200) {
        if ($data->{status} == 404 and
            $data->{error_type} =~ /InstanceNotFoundException/) {
            # special: no error if nothing found
            $data->{value} = {};
        } else {
            dief("Jolokia request failed: %s", $data->{error})
        }
    }
    if (ref($data->{value})) {
        $output = $JSON->encode($data->{value});
    } else {
        # special: wrap scalar values in an array
        $output = $JSON->encode([ $data->{value} ]);
    }
    $output =~ s/\s*$/\n/s;
    if ($Option{output}) {
        file_update($Option{output}, data => $output);
    } else {
        print($output);
    }
}

#
# execute a Jolokia request (high-level)
#

sub jolokia (@) {
    my($url, $type, @args) = @_;
    my(%data);

    dief("invalid url: %s", $url)
        unless $url =~ /^https?:\/\//;
    dief("invalid type: %s", $type)
        unless $type =~ /^\w+$/;
    if ($type eq "version") {
        # https://jolokia.org/reference/html/protocol.html#version
        dief("invalid %s request", $type)
            unless @args == 0;
    } elsif ($type eq "search") {
        # https://jolokia.org/reference/html/protocol.html#search
        dief("invalid %s request", $type)
            unless @args == 1;
        $data{mbean} = $args[0];
    } elsif ($type eq "read") {
        # https://jolokia.org/reference/html/protocol.html#read
        dief("invalid %s request", $type)
            unless 1 <= @args and @args <= 3;
        $data{mbean} = $args[0];
        if (@args >= 2) {
            if ($args[1] =~ /,/) {
                $data{attribute} = [ split(/,/, $args[1]) ];
            } else {
                $data{attribute} = $args[1];
            }
        }
        $data{path} = $args[2]
            if @args == 3;
    } elsif ($type eq "list") {
        # https://jolokia.org/reference/html/protocol.html#list
        dief("invalid %s request", $type)
            unless @args <= 1;
        $data{path} = $args[0]
            if @args == 1;
    } else {
        dief("unsupported type: %s", $type);
    }
    $data{type} = $type;
    execute($url, \%data);
}

#
# just do it
#

init();
jolokia(@ARGV);

__END__

=head1 NAME

jolokia - thin client to access a Jolokia agent

=head1 SYNOPSIS

B<jolokia> [I<OPTIONS>] <URL> <REQUEST>

=head1 DESCRIPTION

This program sends an HTTP POST request to a Jolokia agent (identified by its
<URL>) and prints the response value as JSON.

The <REQUEST> can be:

  version
  search <mbean>
  read <mbean> [<attribute> [<path>]]
  list [<path>]

Where <mbean> is the MBean name or pattern (such as "java.lang:type=Memory" or
"org.apache.activemq:*"), <attribute> is one or more (comma separated)
attribute names and <path> is the pseudo XPath used in Jolokia as documented
at L<https://jolokia.org/reference/html/protocol.html#paths>.

For instance:

  jolokia http://localhost:8000/jolokia read java.lang:type=Threading ThreadCount

Under normal circumstances, the output of this program is always some valid
JSON. This includes when nothing is found (output is C<{}>) or when a single
value is returned by Jolokia (it is then wrapped inside an array).

=head1 OPTIONS

=over

=item B<--debug>, B<-d>

show debugging information

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--manual>, B<-m>

show this manual

=item B<--output>, B<-o> I<PATH>

store the output in the given file

=item B<--pretty>, B<-p>

pretty print JSON

=back

=head1 SEE ALSO

L<https://jolokia.org/reference/html/protocol.html>

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2020-2023
