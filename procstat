#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: procstat                                                               #
#                                                                              #
# Description: display per-process statistics                                  #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use sigtrap qw(die normal-signals);
use Data::Dumper qw(Dumper);
use Getopt::Long qw(GetOptions);
use No::Worries qw($ProgramName);
use No::Worries::Date qw(date_stamp);
use No::Worries::Die qw(dief handler);
use No::Worries::Dir qw(dir_read);
use No::Worries::File qw(file_read file_write);
use No::Worries::Log qw(log_debug log_filter);
use No::Worries::PidFile qw(*);
use No::Worries::Proc qw(proc_detach);
use No::Worries::Syslog qw(syslog_open syslog_close);
use No::Worries::Warn qw(handler);
use Pod::Usage qw(pod2usage);
use POSIX qw();
use Time::HiRes qw();

#
# variables
#

our(%Option, %NeedsCleanup, %PrevStats, %Need, $CLK_TCK, $PAGESIZE, $Header);

#
# initialize
#

sub init () {
    my($status, $message, $code);

    $| = 1;
    $Data::Dumper::Indent = 1;
    $Data::Dumper::Sortkeys = 1;
    $Option{debug} = 0;
    $Option{sleep} = 1;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option,
        "count|c=i",
        "debug|d+",
        "detach=s",
        "fields|f=s",
        "help|h|?",
        "manual|m",
        "pidfile=s",
        "quit",
        "sleep|s=f",
        "status",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    if ($Option{quit}) {
        dief("missing mandatory option for --quit: --pidfile")
            unless $Option{pidfile};
        pf_quit($Option{pidfile},
            callback => sub { printf("%s %s\n", $ProgramName, $_[0]) },
        );
        exit(0);
    }
    if ($Option{status}) {
        dief("missing mandatory option for --status: --pidfile")
            unless $Option{pidfile};
        ($status, $message, $code) =
            pf_status($Option{pidfile}, freshness => 30);
        printf("%s %s\n", $ProgramName, $message);
        exit($code);
    }
    pod2usage(2) unless @ARGV;
    foreach my $pid (@ARGV) {
        dief("invalid pid: %s", $pid) unless $pid =~ /^\d+$/;
    }
    log_filter("debug") if $Option{debug};
    $CLK_TCK = POSIX::sysconf(POSIX::_SC_CLK_TCK());
    log_debug("CLK_TCK = %s", $CLK_TCK);
    $PAGESIZE = POSIX::sysconf(POSIX::_SC_PAGESIZE());
    log_debug("PAGESIZE = %s", $PAGESIZE);
    if ($Option{detach}) {
        $No::Worries::Log::Handler = \&No::Worries::Syslog::log2syslog;
        $No::Worries::Die::Syslog = 1;
        $No::Worries::Warn::Syslog = 1;
        syslog_open(ident => $ProgramName, facility => "user");
        $NeedsCleanup{syslog}++;
        proc_detach(callback => sub {
            printf("%s (pid %d) started\n", $ProgramName, $_[0])
        });
    }
    if ($Option{pidfile}) {
        pf_set($Option{pidfile});
        $NeedsCleanup{pidfile}++;
    }
}

#
# return per-process statistics as one hash
#
# see: http://www.kernel.org/doc/man-pages/online/pages/man5/proc.5.html
#

sub stats ($) {
    my($pid) = @_;
    my(%data, $contents, @list);

    $data{pid} = $pid;
    $data{ts} = Time::HiRes::time();
    if ($Need{date}) {
        $data{date} = date_stamp(int($data{ts} + 0.5));
        $data{time} = substr($data{date}, -8);
    }
    #
    # /proc/<pid>/stat
    #
    if ($Need{proc_stat}) {
        $contents = file_read("/proc/$pid/stat");
        if ($contents =~ /\((.+?)\)/) {
            $data{comm} = $1;
        } else {
            dief("unexpected /proc/%d/stat: %s", $pid, $contents);
        }
        $contents =~ s/\(.+?\)/COMM/;
        @list = split(/\s+/, $contents);
        $data{state}       = $list[2];
        $data{utime}       = $list[13]; # in ticks, including all threads
        $data{stime}       = $list[14]; # in ticks, including all threads
        $data{num_threads} = $list[19];
        $data{vsize}       = $list[22]; # in bytes
        $data{rss}         = $list[23]; # in pages
        # all sizes to be in MB
        $data{vsize} /= 1048576.0;
        $data{rss} *= $PAGESIZE;
        $data{rss} /= 1048576.0;
    }
    #
    # /proc/<pid>/io
    #
    if ($Need{proc_io}) {
        $contents = file_read("/proc/$pid/io");
        foreach my $line (split(/\n/, $contents)) {
            $data{$1} = $2 if $line =~ /^([rw]\w+|\w+[rw]): (\d+)$/;
        }
        # all sizes to be in MB
        foreach my $name (qw(read_bytes write_bytes rchar wchar)) {
            $data{$name} /= 1048576.0;
        }
    }
    #
    # /proc/<pid>/fd/
    #
    if ($Need{proc_fd}) {
        $data{files} = dir_read("/proc/$pid/fd");
    }
    # so far so good...
    return(\%data);
}

#
# sleep for some time but properly handling the pid file (i.e. non blocking)
#

sub mysleep ($) {
    my($duration) = @_;
    my($end, $sleep);

    if ($Option{pidfile}) {
        $end = Time::HiRes::time() + $duration if $duration;
        while (1) {
            if (pf_check($Option{pidfile}) eq "quit") {
                log_debug("told to quit");
                return(0);
            }
            pf_touch($Option{pidfile});
            last unless $duration;
            $sleep = $end - Time::HiRes::time();
            last if $sleep <= 0;
            $sleep = 1 if $sleep > 1;
            Time::HiRes::sleep($sleep);
        }
    } else {
        Time::HiRes::sleep($duration) if $duration;
    }
    return(1);
}

#
# work unit: get and display stats
#

sub work ($@) {
    my($format, @fields) = @_;
    my(%stats, $scale, $contents);

    $contents = $Header if $Option{detach};
    foreach my $pid (@ARGV) {
        $stats{$pid} = stats($pid);
        if ($PrevStats{$pid}) {
            if ($Need{cpu}) {
                # compute CPU percentage
                $scale = ($stats{$pid}{ts} - $PrevStats{$pid}{ts}) * $CLK_TCK
                    / 100;
                $stats{$pid}{scpu} =
                    ($stats{$pid}{stime} - $PrevStats{$pid}{stime}) / $scale;
                $stats{$pid}{ucpu} =
                    ($stats{$pid}{utime} - $PrevStats{$pid}{utime}) / $scale;
            }
            if ($Need{io}) {
                # compute disk I/O (really hitting the disk)
                $scale = $stats{$pid}{ts} - $PrevStats{$pid}{ts};
                $stats{$pid}{rio} =
                    ($stats{$pid}{read_bytes}  - $PrevStats{$pid}{read_bytes})
                    / $scale;
                $stats{$pid}{wio} =
                    ($stats{$pid}{write_bytes} - $PrevStats{$pid}{write_bytes})
                    / $scale;
            }
        } else {
            if ($Need{cpu}) {
                $stats{$pid}{scpu} = 0;
                $stats{$pid}{ucpu} = 0;
            }
            if ($Need{io}) {
                $stats{$pid}{rio} = 0;
                $stats{$pid}{wio} = 0;
            }
        }
        if ($Need{cpu}) {
            $stats{$pid}{cpu} = $stats{$pid}{scpu} + $stats{$pid}{ucpu};
        }
        if ($Need{io}) {
            $stats{$pid}{io} = $stats{$pid}{rio} + $stats{$pid}{wio};
        }
        log_debug("stats = %s", Dumper($stats{$pid})) if $Option{debug} > 1;
        if ($Option{detach}) {
            $contents .= sprintf($format, map($stats{$pid}{$_}, @fields));
        } else {
            printf($format, map($stats{$pid}{$_}, @fields));
        }
    }
    if ($Option{detach}) {
        # almost atomic write...
        file_write("$Option{detach}.tmp", data => $contents);
        rename("$Option{detach}.tmp", $Option{detach})
            or dief("canot rename(%s, %s): %s", "$Option{detach}.tmp",
                    $Option{detach}, $!);
    }
    %PrevStats = %stats;
}

#
# cleanup
#

END {
    return if $No::Worries::Proc::Transient;
    pf_unset($Option{pidfile}) if $NeedsCleanup{pidfile};
    syslog_close() if $NeedsCleanup{syslog};
    unlink("$Option{detach}.tmp", $Option{detach}) if $Option{detach};
}

#
# main work loop
#

sub main () {
    my(%metric, @fields, @format, @header, $format);

    %metric = (
        "date"    => [ "date",        "%-19s", "DATE               " ],
        "time"    => [ "time",         "%-8s", "TIME    " ],
        "ts"      => [ "ts",        "%-14.3f", "TIMESTAMP     " ],
        "pid"     => [ "pid",           "%5d", "  PID" ],
        "state"   => [ "state",         "%5s", "STATE" ],
        "files"   => [ "files",         "%6d", " FILES" ],
        "threads" => [ "num_threads",   "%7d", "THREADS" ],
        "cpu"     => [ "cpu",         "%6.1f", "  %CPU" ],
        "scpu"    => [ "scpu",        "%6.1f", " %UCPU" ],
        "ucpu"    => [ "ucpu",        "%6.1f", " %SCPU" ],
        "io"      => [ "io",          "%5.1f", "  I/O" ],
        "rio"     => [ "rio",         "%5.1f", " RI/O" ],
        "wio"     => [ "wio",         "%5.1f", " WI/O" ],
        "rss"     => [ "rss",         "%7.1f", "    RSS" ],
        "vsz"     => [ "vsize",       "%7.1f", "    VSZ" ],
    );
    $Option{fields} ||= "date,pid,threads,cpu,io,rss,vsz";
    $Option{fields} =
        "date,ts,pid,state,files,threads,cpu,scpu,ucpu,io,rio,wio,rss,vsz"
        if $Option{fields} eq "ALL";
    foreach my $name (split(/\s*,\s*/, $Option{fields})) {
        dief("invalid field: %s", $name) unless $metric{$name};
        push(@fields, $metric{$name}->[0]);
        push(@format, $metric{$name}->[1]);
        push(@header, $metric{$name}->[2]);
    }
    $Need{date}++ if grep(/^(date|time)$/, @fields);
    $Need{cpu}++ if grep(/^[su]?cpu$/, @fields);
    $Need{io}++ if grep(/^[rw]?io$/, @fields);
    $Need{proc_stat}++ if $Need{cpu}
        or grep(/^(state|num_threads|rss|vsize)$/, @fields);
    $Need{proc_io}++ if $Need{io};
    $Need{proc_fd}++ if grep(/^(files)$/, @fields);
    $format = join("  ", @format) . "\n";
    $Header = join("  ", @header) . "\n";
    print($Header) unless $Option{detach};
    if ($Option{count}) {
        foreach my $iter (1 .. $Option{count}) {
            work($format, @fields);
            if ($iter != $Option{count}) {
                mysleep($Option{sleep}) or last;
            }
        }
    } else {
        while (1) {
            work($format, @fields);
            mysleep($Option{sleep}) or last;
        }
    }
}

#
# just do it
#

init();
main();

__END__

=head1 NAME

procstat - display per-process statistics

=head1 SYNOPSIS

B<procstat> [I<OPTIONS>] I<PID>...

=head1 DESCRIPTION

B<procstat> displays statistics for the given process ids.

By default, B<procstat> will display new statistics every second,
until it is killed. This can be changed with the B<--count> and
B<--sleep> options.

For instance:

  $ procstat `pidof httpd`
  DATE                   PID  THREADS   %CPU    I/O      RSS      VSZ
  2011/09/06-09:25:35  22402        1    0.1    0.0      6.4    185.4
  2011/09/06-09:25:35   3679        1    0.0    0.0      6.3    178.3
  2011/09/06-09:25:36  22402        1    0.2    0.0      6.4    185.4
  2011/09/06-09:25:36   3679        1    0.0    0.0      6.3    178.3
  2011/09/06-09:25:37  22402        1    0.1    0.4      6.4    185.4
  2011/09/06-09:25:37   3679        1    0.0    0.0      6.3    178.3
  ^C

=head1 OPTIONS

=over

=item B<--count>, B<-c> I<INTEGER>

display only this number of statistics;
default: unlimited

=item B<--debug>, B<-d>

show debugging information

=item B<--detach> I<PATH>

detach B<procstat> so that it becomes a daemon running in the background;
its normal output (with column headers) is saved into the given file,
which gets overwritten at each iteration

=item B<--fields>, B<-f> I<STRING>

display this comma separated list of fields;
default: C<date,pid,threads,cpu,io,rss,vsz>

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--manual>, B<-m>

show this manual

=item B<--pidfile> I<PATH>

use this pid file

=item B<--quit>

tell another instance of B<procstat> (identified by its pid file, as
specified by the B<--pidfile> option) to quit

=item B<--sleep>, B<-s> I<NUMBER>

sleep this number of seconds (can be fractional) between statistics;
default: 1

=item B<--status>

get the status of another instance of B<procstat> (identified by its
pid file, as specified by the B<--pidfile> option); the exit code will
be zero if the instance is alive and non-zero otherwise

=back

=head1 FIELDS

Here are the fields that can be selected:

=over

=item *

C<cpu>: total CPU percentage

=item *

C<date>: date string such as C<2011/09/06-09:25:35>

=item *

C<files>: number of open files

=item *

C<io>: total disk I/O in MB/s

=item *

C<pid>: process id

=item *

C<rio>: read part of the total disk I/O in MB/s

=item *

C<rss>: physical memory used (Resident Set Size) in MB

=item *

C<scpu>: system part of the CPU percentage

=item *

C<state>: state character like top(1) reports

=item *

C<time>: time string such as C<09:25:35>

=item *

C<threads>: number of threads

=item *

C<ts>: fractional Unix timestamp in seconds since the Epoch

=item *

C<ucpu>: user part of the CPU percentage

=item *

C<vsz>: virtual memory used (Virtual Set Size) in MB

=item *

C<wio>: write part of the total disk I/O in MB/s

=back

You can also use B<--fields ALL> to display all fields.

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2011-2023
