%define real_version %((cat %{_sourcedir}/VERSION || cat %{_builddir}/VERSION || echo UNKNOWN) 2>/dev/null)

Name:		mig-broker-utils
Version:	%{real_version}
Release:	1%{?dist}
Summary:	Tools to be used on the MIG brokers
License:	Apache-2.0
URL:		https://gitlab.cern.ch/mig/%{name}
Source0:	%{name}-%{version}.tgz
Source1:	VERSION
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch:	noarch
BuildRequires:	perl

%description
This package contains a set of tools to be used on the MIG brokers.

%prep
%setup -q -n %{name}-%{version}

%build
make build

%install
make install INSTROOT=%{buildroot} BINDIR=%{_bindir} MANDIR=%{_mandir}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README CHANGES
%{_bindir}/*
%{_mandir}/man?/*
